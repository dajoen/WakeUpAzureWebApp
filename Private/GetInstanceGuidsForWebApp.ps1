# Get all instanceGuids currently active for this $appName
function GetInstanceGuidsForWebApp($webApp) {

    if ($webApp -eq $null) { throw "webApp parameter is null"; }

    $appName = $webApp.Name;
    Write-Host "Getting all instance id's for webApp $appName";
    $instances = Get-AzureRmResource -ResourceGroupName $webApp.ResourceGroup -ResourceType Microsoft.Web/sites/instances -Name $appName;

    return $instances | ForEach-Object { return $_.Name.ToString(); };
}