# Add default hostname to a list of relative urls
function  MapUrlsWithHostName([string]$defaultHostName, [string[]]$urls){

    # Assert parameters
    if ([string]::IsNullOrWhiteSpace($defaultHostName)) { throw "defaultHostName parameter is null or empty"; }
    if ($urls -eq $null) { throw "urls parameter is null"; }

    return $urls | Foreach-Object {

        # Make sure no trailing slashes come through
        $path = $_.Trim('/');

        # Combine url and path
        $compositeUrl = Join-Path -Path $defaultHostName -ChildPath $_;
        
        # Fix slashes
        $compositeUrl = $compositeUrl.Replace('\', '/');
        $compositeUrl = $compositeUrl.Trim('/');
        # Make sure path ends with a slash, except if no path
        if (![string]::IsNullOrWhiteSpace($path)) { $compositeUrl = "$compositeUrl/" }

        return "https://$compositeUrl";
    };
}