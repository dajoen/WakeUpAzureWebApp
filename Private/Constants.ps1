
$Constants = @{

    Defaults = @{
        RequestTimeOut = New-TimeSpan -Seconds 20;
        ConsecutiveErrorThreshold = 10;
        NoResponseRetries = 3;
    };

    TimeSpanFormat = "hh\:mm\:ss\.fff";
    InvokeWebRequestTimeOutMessage = "The operation has timed out.";

    FilteredErrorMessages = @(

        # Azure WebApp Stopped or Starting
        "Site Disabled";
        "Site Unavailable from Mini-ARR";
        "Service Unavailable";

        # Dll Faillure
        "HTTP Error 502.5 - Process Failure";
    )

};