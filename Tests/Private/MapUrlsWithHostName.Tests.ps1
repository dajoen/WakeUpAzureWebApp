$PSScriptRoot = "$PSScriptRoot/../../";
. "$PSScriptRoot/Private/MapUrlsWithHostName";

Describe "MapUrlsWithHostName" {
    Mock -CommandName Write-Host -Verifiable -MockWith { };
    Mock -CommandName Write-Output -Verifiable -MockWith { };

    It "SingleUrl" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = "/page";

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should Be "https://$defaultHostName/page/";

    }

    It "RootUrl HasNoTrailing /" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = "/";

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should Be "https://$defaultHostName"

    }

    It "MultiUrl" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = @(
            "/"
            "/page-a/"
            "/page-b"
        );

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should Be @(
            "https://$defaultHostName"
            "https://$defaultHostName/page-a/"
            "https://$defaultHostName/page-b/"
        );

    }

    It "MissingParameters ShouldFail" {

        # Act
        $noDefaultHostName = { MapUrlsWithHostName -defaultHostName "hostname"; }
        $noUrls = { MapUrlsWithHostName -urls "/"; }
        $none = { MapUrlsWithHostName; }

        # Assert
        $noDefaultHostName | Should Throw "urls parameter is null";
        $noUrls | Should Throw "defaultHostName parameter is null or empty";
        $none | Should Throw "defaultHostName parameter is null or empty";

    }
}