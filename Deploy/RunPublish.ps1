# Actually push package
$manifest = Get-Content "./WakeUpAzureWebApp.psd1";
$manifest = $manifest.replace("#{ModuleVersion}", "$env:CI_BUILD_TAG");
$manifest | Set-Content "./WakeUpAzureWebApp.psd1"

Publish-Module `
    -Path "./" `
    -NuGetApiKey "$env:NuGetApiKey";