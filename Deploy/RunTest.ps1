Write-Host "Installing dependencies";

Install-Module Pester `
    -Scope CurrentUser `
    -Force `
    -SkipPublisherCheck; 

try {
    Install-Module AzureRm `
        -Scope CurrentUser `
        -Force `
        -SkipPublisherCheck;
} catch { }

# Run test
Write-Host "Invoking Tests";
Write-Host;

Invoke-Pester `
    -EnableExit;