import-module "$PSScriptRoot/WakeUpAzureWebApp.psm1" -Force -DisableNameChecking

# Example usage file, the webapp used in this example might not be up.

$targetWebAppName = "wakeup-example";
$warmupUrls = @(

  # 200
  "/";
  "someurl/";
  "someotherurl/";

  # 3xx
  "301";
  "302";

  # 4xx
  "not-existing/";
  "forbidden/";

  # 5xx
  "error/";
  "badgateway/";
);

Start-WakeUpAzureWebApp `
  -TargetWebAppName $targetWebAppName `
  -WarmupUrls $warmupUrls; # `
  # -RequestTimeOut (New-TimeSpan -Seconds 1) `
  # -NoResponseRetries 1 `
  # -ConsecutiveErrorThreshold 1 `
  # -TotalErrorThreshold 1 `
  # -UseExitInsteadOfThrow `
  # -OutputHtml;